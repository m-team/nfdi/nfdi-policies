---
documentclass: article
title: Template Datenschutzerklärung gem. Art. 13 DSGVO
colorlinks: true
fontsize: 11pt
geometry: margin=20mm
papersize: a4
numbersections: true
header-includes:
 - # font
 - \renewcommand{\familydefault}{\sfdefault}

 - # linespacing
 - \usepackage{setspace}
 - \linespread{1.0}
 - \pagestyle{empty}

 - # footer
 - \pagenumbering{gobble}
 - \usepackage[markcase=noupper]{scrlayer-scrpage}
 - \cfoot*{\scriptsize\textcolor{gray}{Author{:} DFN-CERT Services GmbH}}

 - # logo on top right of first page
 - \usepackage{graphicx}
 - \graphicspath{{images/}}
 - \ohead*{\vspace{-18mm}\includegraphics[width=68.8mm,height=24.6mm]{nfdi-logo.png}}
 - \ohead{}
---
<!-- Add line numbers with: -->
<!-- - \usepackage{lineno} -->
<!-- - \linenumbers -->

<!-- Compile with:
pandoc <filename.md> -o <filename.pdf>
pandoc <filename.md> -o <filename.docx> --reference-doc style.docx
pandoc <filename.md> -o <filename.odt> ## missing header and footer!
-->

(Version 0.9.4, August 19^th^, 2024)

# Name des Dienstes

Bitte tragen Sie eine Bezeichnung für den Dienst ein, anhand derer dieser
Dienst eindeutig identifiziert werden kann.


# Beschreibung des Dienstes

Eine kurze Beschreibung ist ausreichend, die den Kern des Dienstes trifft.


# Verantwortlicher

\'Verantwortlicher\' ist die natürliche oder juristische Person, Behörde,
Einrichtung oder andere Stelle, die allein oder gemeinsam mit anderen über
die Zwecke und Mittel der Verarbeitung von personenbezogenen Daten entscheidet.
Bitte benennen Sie hier den Verantwortlichen mit Name Anschrift und ggf.
vertretungsberechtigte Personen.
<!--
(Vorstand, Geschäftsführer, Kanzler, Präsident, ...).
ich würde diese Beispiele weglassen, weil das sonst dazu führt, dass die Rechtsabteilung eingeschaltet wird
-->


# Verarbeitung personenbezogener Daten

**Verarbeitungszwecke, Rechtsgrundlagen und Kategorien personenbezogener Daten**

Bitte spezifizieren Sie die
- Kategorien (z.B. Personendaten, Anschrift, Kontaktdaten, etc.)
    personenbezogener Daten, die für den jeweiligen Zweck verarbeitet
    werden,
- die Zwecke der Verarbeitung personenbezogener Daten,
- die für jeden Verarbeitungszweck geltende Rechtsgrundlage.

Ausgangsbasis für die Rechtsgrundlage sind grundsätzlich Art. 6(1) (für
"normale" personenbezogene Daten) oder Art. 9(1) DSGVO (für besondere
Kategorien von personenbezogenen Daten wie z.B. Gesundheitsdaten,
Religionszugehörigkeit, etc.).


**Sonderfälle:**

- Erfolgt die Datenverarbeitung aufgrund Art. 6(1), Buchstabe f) DSGVO,
  muss das berechtigte Interesse des Verantwortlichen oder eines Dritten
  dargelegt werden.
- Falls zutreffend, die Angabe, ob die Datenverarbeitung der Erfüllung
  eines Vertrags dient, dessen Vertragspartei die betroffene Person ist,
  oder zur Durchführung vorvertraglicher Maßnahmen erforderlich ist, die
  auf Anfrage der betroffenen Person erfolgen, sowie die möglichen
  Konsequenzen, wenn diese Daten nicht zur Verfügung gestellt werden.


# Weitergabe personenbezogener Daten an Dritte

_Sofern zutreffend:_ Nennung dritter juristischer und/oder natürlicher Personen,
an die personenbezogene Daten übermittelt werden. Eine Übermittlung liegt
bereits vor, wenn Dritte die Daten einsehen können. Es muss somit nicht
notwendigerweise eine Speicherung bei einem Dritten erfolgen.


# Übermittlungen personenbezogener Daten an Drittländer oder an internationale Organisationen außerhalb des Geltungsbereichs der DSGVO

_Sofern zutreffend:_ Der Hinweis des Verantwortlichen, dass er beabsichtigt,
personenbezogene Daten in ein Drittland oder an eine internationale Organisation
zu übermitteln sowie das Vorhandensein oder das Fehlen eines
Angemessenheitsbeschlusses der Kommission oder im Falle von Übermittlungen gemäß
Artikel 46 oder Artikel 47 oder Artikel 49 Absatz 1 Unterabsatz 2 einen Verweis
auf die geeigneten oder angemessenen Garantien und die Möglichkeit, wie eine Kopie
von ihnen zu erhalten ist, oder wo sie verfügbar sind. Drittländer sind Länder in
denen die DSGVO nicht gilt.

_Andernfalls:_ Keine Stellungnahme erforderlich

# Speicherfristen

Die Dauer, für die die personenbezogenen Daten gespeichert werden oder,
falls dies nicht möglich ist, die Kriterien für die Festlegung dieser Dauer.


# Rechte betroffener Personen und wie diese geltend gemacht werden können

Gemäß Kapitel III (Artikel 12-23) der Verordnung (EU) 2016/679, verfügen Sie
als betroffene Person über bestimmte Rechte, insbesondere das Recht, auf
Auskunft über die Sie betreffenden verarbeiteten personenbezogenen Daten
sowie das Recht auf Berichtigung dieser Daten, sofern diese unzutreffend
oder unvollständig sind.
Abgesehen von bestimmten Ausnahmen haben Sie das Recht auf die Löschung
der Sie betreffenden personenbezogenen Daten, die Einschränkung der
Verarbeitung dieser Daten zu verlangen, der Verarbeitung zu widersprechen,
das Recht auf Datenübertragbarkeit, sowie das Recht, nicht einer ausschließlich
auf einer automatisierten Verarbeitung - einschließlich Profiling - beruhenden
Entscheidung unterworfen zu werden, die Ihnen gegenüber rechtliche Wirkung
entfaltet oder Sie in ähnlicher Weise erheblich beeinträchtigt.

Sie können diese Recht geltend machen, indem Sie den Verantwortlichen kontaktieren,
oder - im Konfliktfall - den/die zuständigen Datenschutzbeauftragten.


# Widerruf der Einwilligung

_Nur zutreffend, sofern die Datenverarbeitung aufgrund von Atrikel 6(1)
Buchstabe (a) oder Atrikel 9(2) Buchstabe (a) erfolgt:_
Das Recht, die Einwilligung in die Verarbeitung personenbezogener Daten
jederzeit zu widerrufen. Durch den Widerruf der Einwilligung wird die
Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten
Verarbeitung nicht berührt.

_Andernfalls:_ nicht zutreffend


# Datenübertragbarkeit

Nur zutreffend, wenn die Verarbeitung auf einer Einwilligung gemäß Artikel 6(1)
Buchstabe (a) oder Artikel 9(2) Buchstabe (a) oder auf einem Vertrag gemäß
Artikel 6(1) Buchstabe (b) beruht und die Verarbeitung mithilfe automatisierter
Verfahren erfolgt.

_Andernfalls:_ nicht zutreffend


# Recht auf Beschwerde bei einer Aufsichtsbehörde

Jede betroffene Person hat unbeschadet eines anderweitigen verwaltungsrechtlichen
oder gerichtlichen Rechtsbehelfs das Recht auf Beschwerde bei einer Aufsichtsbehörde,
wenn die betroffene Person der Ansicht ist, dass die Verarbeitung der sie
betreffenden personenbezogenen Daten gegen die DSGVO verstößt.

_Bitte die für den Verantwortlichen zuständige Datenschutzaufsichtsbehörde benennen, inkl. Kontaktdaten_


# Datenschutzbeauftrage(e) oder zuständige Kontaktperson des Verantwortlichen

_Sofern zutreffend:_ Bitte bennenen Sie den/die Datenschutzbeauftragten
inklusive Kontaktdaten.

_Falls die Bestellung eines Datenschutzbeauftragten nicht erforderlich sein sollte:_
Bitte benennen Sie eine für Datenschutzfragen zuständige Kontaktperson.

# Information über automatisierte Entscheidungsfindung

_Sofern zutreffend:_ Bestehen einer automatisierten Entscheidungsfindung einschließlich
Profiling gemäß Artikel 22 (1) und (4) und - zumindest in diesen Fällen - aussagekräftige
Informationen über die involvierte Logik sowie die Tragweite und die angestrebten
Auswirkungen einer derartigen Verarbeitung für die betroffene Person.

_Andernfalls:_ Keine Angaben erforderlich

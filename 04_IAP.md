---
documentclass: article
title: Infrastructure Attribute Profiles
colorlinks: true
fontsize: 11pt
geometry: margin=20mm
papersize: a4
numbersections: true
header-includes:
    - # font
    - \renewcommand{\familydefault}{\sfdefault}

    - # linespacing
    - \usepackage{setspace}
    - \linespread{1.0}
    - \pagestyle{empty}

    - # footer
    - \pagenumbering{gobble}
    - \usepackage[markcase=noupper]{scrlayer-scrpage}
    - \cfoot*{\scriptsize\textcolor{gray}{{© Owned by the authors and made available under license{:} \href{https://creativecommons.org/licenses/by-nc-sa/4.0/}{https://creativecommons.org/licenses/by-nc-sa/4.0/} \\ Other Sources / Attribution / Acknowledgements{:} "The AARC Policy on the Processing of Personal Data", under CC BY-NC-SA 4.0 }}}

    - # logo on top right of first page
    - \usepackage{graphicx}
    - \graphicspath{{images/}}
    - \ohead*{\vspace{-18mm}\includegraphics[width=68.8mm,height=24.6mm]{nfdi-logo.png}}
    - \ohead{}
---
<!-- Add line numbers with: -->
<!-- - \usepackage{lineno} -->
<!-- - \linenumbers -->

<!-- Compile with:
pandoc <filename.md> -o <filename.pdf>
pandoc <filename.md> -o <filename.docx> --reference-doc style.docx
pandoc <filename.md> -o <filename.odt>  ## missing header and footer!
-->

(Version 0.9.4, August 19^th^, 2024)

# INTRODUCTION

Attributes are used to describe the user. They are meant to be used for
the authorisation decision and - if applicable - to identify the user
and personalise a service. Different sets of attributes are available at
different layers in the overarching infrastructure, cf. NFDI-AAI
Architecture \[[[NFDI-Arch]{.underline}](#references)\]. For example,
services connected to a national identity federation in eduGAIN
\[[[eduGAIN]{.underline}](\l)\], like e.g. DFN-AAI, cannot 'see' those
attributes that are managed within a Community AAI.

Therefore, the set of attributes coming from *Home IdPs* in eduGAIN or
the edu-ID Proxy \[[[eduID]{.underline}](\l)\] is different to the one
that is passed on by the *Community AAIs*. The set of attributes
available from the NFDI Infrastructure Proxy (cf. NFDI-AAI Architecture
\[[[NFDI-Arch]{.underline}](#references)\]) will be very similar to
those of the *Community AAIs* .

The NFDI-AAI makes use of standardised attribute sets, both for the
attributes expected from the Home IdPs at the *Community AAIs*, as well
as for those that are sent from the Community AAIs to underlying
services.

The attribute sets specified by the profiles below are based on several
guidelines of the AARC Community \[[[AARC]{.underline}](#references)\]
that are widely accepted and used in international research
collaborations as well as in the upcoming EOSC AAI.

# DEFINITIONS

- *Attribute*: Any information relating to a natural person.

- *Community AAI*: An Authentication and Authorisation Infrastructure
    (AAI) operated by or on behalf of a research community. Its central
    element is a *Service Provider to Identity Provider proxy* (SP-IdP
    proxy), a single component that negotiates between *Home IdPs* and
    the community services behind the proxy, shielding those services
    from the heterogeneity of global identity federations.

- *Home IdP*: An *Identity Provider* operated by or on behalf of the
    user's Home Organisation.

- *Identity Provider:* An Identity Provider (IdP) is a system that
    creates, maintains, and manages identity information for Users while
    offering authentication functionality to relying Services and SP-IdP
    proxy within the Infrastructure.

# Federation Attribute Profile Attributes required by Community AAIs

This attribute profile specifies the attributes that are required to be
released by the Home-IdPs and the edu-ID Proxy, so that users can
reasonably use the services at the Community AAI. Precise requirements
may differ between different Instances and Software Products used to
implement a Community AAI.

The Federation Attribute Profile consists abstractly of the following
data elements:

- Home Organisation

- User Identifier

- Person Name

- Email Address

- Affiliation

- Assurance

This profile is fully covered by the **REFEDS Personalized Access Entity
Category**

([[https://refeds.org/category/personalized]{.underline}](https://refeds.org/category/personalized)):


:::{.list-table header-rows=1 widths=2,3,3}
  * - **Identity Attribute Type**
    - **SAML Attribute**
    - **OpenID Connect Claim**

  * - Home Organisation
    - schacHomeOrganization
    - One of:
        - org_domain
        - schac\_home\_organization

  * - User Identifier
    - subject-id
    - sub (shared) + iss

  * - Person Name
    - All of:
        - displayName
        - givenName
        - sn

    - All of:
        - name
        - given\_name
        - family\_name

  * - Email Address
    - mail
    - email

  * - Affiliation
    - eduPersonScopedAffiliationx
    - eduperson\_scoped\_affiliation

  * - Assurance
    - eduPersonAssurance
    - One of:
        - eduperson\_assurance
        - asr
:::

For an overview of the SAML Attribute Schemas, please refer to
[[https://doku.tid.dfn.de/de:attributes]{.underline}](https://doku.tid.dfn.de/de:attributes)
and \[[[eduPerson]{.underline}](#references)\]. As for the Assurance
information, the REFEDS Assurance Framework is authoritative:
[[https://refeds.org/assurance]{.underline}](https://refeds.org/assurance),
cf. also
[[https://doc.nfdi-aai.de/assurance/]{.underline}](https://doc.nfdi-aai.de/assurance/).
The OpenID Connect claims *sub, iss, email, name, given_name,
family_name* are part of the OpenID Connect Core Specification
\[[[OIDC-Core]{.underline}](#references)\].

If the Community AAI provides a means to query the user for a Name
(displayName, or givenName + sn), and a (verified!) email address, the
attribute bundle of the The **REFEDS Pseudonymous Access Entity
Category** may also be applicable
([[https://refeds.org/category/pseudonymous]{.underline}](https://refeds.org/category/pseudonymous)):



:::{.list-table header-rows=1 widths=2,3,3}
  * - **Identity Attribute Type**
    - **SAML Attribute**
    - **OpenID Connect Claim**

  * - Home Organisation
    - schacHomeOrganization
    - One of:
        - org\_domain
        - schac\_home\_ogranization

  * - User Identifier (pseudonymous/pairwise)
    - pairwise-id
    - sub (pairwise) + iss

  * - Affiliation
    - eduPersonScopedAffiliation
    - eduperson\_scoped\_affiliation

  * - Assurance
    - eduPersonAssurance
    - One of:
        - eduperson\_assurance
        - asr
:::


# Community Attribute Profiles Attributes available from the Community AAIs

A "Core" and an "Extended" Attribute Profile are available to services
that are deployed within Community AAIs, i.e. community services
connected to the IdP component of a Community AAI's SP-IdP proxy or of
the NFDI Infrastructure Proxy.

## Core Attribute Profile

Attributes listed here are **mandatory**.

The table uses this convention:

- **bold entries** in the table are the preferred attributes.

- Normal (non-bold) entries are possible alternatives to the bold
    entries. They may be permissible whenever multiple attributes are
    sent.

- Receiving ends may ignore some of the non-bold entries and should
    use the bold ones.


:::{.list-table header-rows=1 widths=2,5,5,3}
  * - **Identity Attribute Type**
    - **SAML Attribute**
    - **OpenID Connect Claim**
    - **Comments**
  * - Non-reassignable, persistent, unique user identifier
    - Any of the following:
        - **voPersonID**        [voperson]
        - eduPersonUniqueId     [eduperson]
        - subject-id            [SAML-SubjectID]
        - pairwise-id           [SAML-SubjectID]
        - SAML Persistent       NameID [SAML2Core]
    - Any of the following:
        - **voperson_id**  [voperson]
        - eduperson_unique_id [eduperson]
        - sub (public)+iss     [OIDC-Core]
        - sub (pairwise)+iss   [OIDC-Core]
    - Created by the Community AAI
  * - Name information
    - **displayName**
    - **name** [OIDC-Core]
    - E.g.: John Doe
  * - Email information
    - Any of
        - mail
        - **voPersonVerifiedEmail**
    - Any of
        - email [OIDC-Core]
        - email_verified [OIDC-Core]
        - **voperson_verified_email** [voPerson-2.0]
    - There is currently no way of indicating a preferred email address (e.g. when sending multiple emails). One workaround may be to use the **first** entry of the list as a **preferred** email address of the user. This **MAY NOT work in all circumstances!!!**
  * - Home organisation
    - **schacHomeOrganization**
    - Either of
        - **org_domain** + org_name
        - schac_home_organization
    - The domain name of the users Home-Org.
  * - Affiliation within the community
     - **eduPersonScopedAffiliation** [eduPerson]
     - **eduperson_scoped_affiliation** [eduPerson]
     - A controlled vocabulary will be provided by NFDI-AAI (following EOSC/AARC conventions)

  * - Affiliation at the Home-Org. Assurance
    - **voPersonExternalAffiliation** [voPerson]
    - **voperson_external_affiliation** [voPerson]
    - Home-Org. Affiliation will be passed on "as is" in this attribute

  * - Assurance
    - **eduPersonAssurance**     [eduPerson]
    - **eduperson_assurance**    [eduPerson]
    - As defined in [RAF], and detailed [here](assurance.md).
:::


## Extended Attribute Profile

Attributes listed here are optional.


:::{.list-table header-rows=1 widths=2,5,5,3}
  * - **Identity Attribute Type**
    - **SAML Attribute**
    - **OpenID Connect Claim**
    - **Comments**

  * - Groups and roles
    - eduPerson  Entitlement [[AARC-  G002](#references)]
    - One of the following:
        - edup erson_entitlement [[AARC -G002](#references)]
        - entitlements [[RFC9068](#references), [AARC-G069](#references)]
    - urn:geant:dfn.de:nfdi. de:**group**:example#au thority.host.de (indicates a group membership) Note: The authority part is optional. Still in NFDI-AAI we want to use it. A registry is operated at [https://www.nfdi.de/p ersistent-identifier s](https://www.nfdi.de/pers istent-identifiers).

  * - Capabilities
    - eduPerson  Entitlement [[AARC-G027](#references)]
    - any of the following:
          - eduperson_entitlement [[aarc-g027](#references)]
          - entitlements  [[rfc9068](#references), [aarc-g027](#references)]
    - urn:geant:dfn.de:nfdi .de:**res**:example#au thority.host.de (indicates a resource entitlement). A registry is operated at [https://www.nfdi.de/persistent-identifiers](https://www.nfdi.de/persistent-identifiers).


  * - Agreement to policies
    - voPersonPolicyAgreement [[voPerson]](#references)
    - voperson_policy_agreement [[voPerson](\l)]
    - Allows services to skip local policy clicking, if e.g. done at Community-AAI

  * - ORCID identifier
    - eduPersonOrcid [[eduPerson](\l)]
    - orcid
    - [https://orcid.org](https://orcid.org)

  * - Preferred email
    - ?
    - ?
    - Unclear if this will be used, since EOSC/AARC directions are unclear at the moment (i.e. August 2023).

  * - Supplemental Name Information
    - givenName + sn [[eduPerson](\l)]
    - given_name + family_name [[OIDC-Core](#references)]

  * - Authentication Profiles
    - AuthnContextClassRef [[SAML2 Core](#references)]
    - acr
    - For indicatingwhether a 2nd factor was used for

  * - External Identifier
    - voPersonExternalID [[voPerson]](# references)
    - voperson_external_id [[voPerson]](#references)
    - An explicitly scoped identifier for a person, typically as issued by an external authentication service. Could be used for ID linking.

  * - SSH Keys
    - sshPublicKey
    - ssh_public_key
    - A list of ssh keys

:::

# References

\[AARC\] AARC Guidelines -
[[https://aarc-community.org/guidelines/]{.underline}](https://aarc-community.org/guidelines/)

\[AARC-G002\] AARC-G002 Expressing group membership and role information
-\
[[https://aarc-community.org/guidelines/aarc-g002]{.underline}](https://aarc-community.org/guidelines/aarc-g002)

\[AARC-G027\] AARC-G027 Guidelines for expressing resource capabilities
-\
[[https://aarc-community.org/guidelines/aarc-g027/]{.underline}](https://aarc-community.org/guidelines/aarc-g027/)

\[AARC-G069\] AARC-G069 Guidelines for expressing group membership and
role information -\
[[https://aarc-community.org/guidelines/aarc-g069/]{.underline}](https://aarc-community.org/guidelines/aarc-g069/)

\[eduGAIN\] eduGAIN -
[[https://technical.edugain.org/status]{.underline}](https://technical.edugain.org/status)

\[eduID\] DFN edu-ID -
[[https://doku.tid.dfn.de/de:aai:eduid:start]{.underline}](https://doku.tid.dfn.de/de:aai:eduid:start)

\[eduPerson\] eduPerson and other common attributes -\
[[https://wiki.refeds.org/display/STAN/eduPerson+%28202208%29+v4.4.0]{.underline}](https://wiki.refeds.org/display/STAN/eduPerson+%28202208%29+v4.4.0)

\[NFDI-Arch\] NFDI-AAI Architecture -
[[https://doc.nfdi-aai.de/architecture/]{.underline}](https://doc.nfdi-aai.de/architecture/)

\[OIDC-Core\] OpenID Connect Core -
[[https://openid.net/specs/openid-connect-core-1_0.html]{.underline}](https://openid.net/specs/openid-connect-core-1_0.html)

\[RAF\] REFEDS Assurance Framework -
[[https://refeds.org/assurance]{.underline}](https://refeds.org/assurance)

\[RFC9068\] RFC 9068, JSON Web Token (JWT) Profile for OAuth 2.0 Access
Tokens\
[[https://datatracker.ietf.org/doc/html/rfc9068]{.underline}](https://datatracker.ietf.org/doc/html/rfc9068)

\[SAML2Core\] OASIS Standard, Assertions and Protocols for the OASIS
Security Assertion Markup\
Language (SAML) V2.0, March 2005.[\
[http://docs.oasis-open.org/security/saml/v2.0/saml-core-2.0-os.pdf]{.underline}](http://docs.oasis-open.org/security/saml/v2.0/saml-core-2.0-os.pdf)

\[SAML-SubjectID\] SAML V2.0 Subject Identifier Attributes Profile Version 1.0\
[[https://docs.oasis-open.org/security/saml-subject-id-attr/v1.0/saml-subject-id-attr-v1.0.html]{.underline}](https://docs.oasis-open.org/security/saml-subject-id-attr/v1.0/saml-subject-id-attr-v1.0.html)

\[SCHAC\] SCHema for ACademia -
[[https://wiki.refeds.org/display/STAN/SCHAC+Releases]{.underline}](https://wiki.refeds.org/display/STAN/SCHAC+Releases)

\[voPerson\] voPerson Schema 2.0 -\
[[https://github.com/voperson/voperson/blob/draft-2.0.0/voPerson.md]{.underline}](https://github.com/voperson/voperson/blob/draft-2.0.0/voPerson.md)

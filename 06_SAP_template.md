---
documentclass: article
title: Service Access Policy Template
colorlinks: true
fontsize: 11pt
geometry: margin=20mm
papersize: a4
numbersections: true
header-includes:
    - # font
    - \renewcommand{\familydefault}{\sfdefault}

    - # linespacing
    - \usepackage{setspace}
    - \linespread{1.0}
    - \pagestyle{empty}

    - # footer
    - \pagenumbering{gobble}
    - \usepackage[markcase=noupper]{scrlayer-scrpage}
    - \cfoot*{\scriptsize\textcolor{gray}{{© Owned by the authors and made available under license{:} \href{https://creativecommons.org/licenses/by-nc-sa/4.0/}{https://creativecommons.org/licenses/by-nc-sa/4.0/} \\ Other Sources / Attribution / Acknowledgements{:} "The AARC Policy on the Processing of Personal Data", under CC BY-NC-SA 4.0 }}}

    - # logo on top right of first page
    - \usepackage{graphicx}
    - \graphicspath{{images/}}
    - \ohead*{\vspace{-18mm}\includegraphics[width=68.8mm,height=24.6mm]{nfdi-logo.png}}
    - \ohead{}
---
<!-- Add line numbers with: -->
<!-- - \usepackage{lineno} -->
<!-- - \linenumbers -->

<!-- Compile with:
pandoc <filename.md> -o <filename.pdf>
pandoc <filename.md> -o <filename.docx> --reference-doc style.docx
pandoc <filename.md> -o <filename.odt>  ## missing header and footer!
-->

(Version 0.9.4, August 19^th^, 2024)

*When using the baseline SAP text below, curly brackets \"{ }\" (colored
blue) indicate text which should be replaced as appropriate to the
requirements of the Service.*

This policy is effective from {insert date}.

# Service Access Policy

This Service Access Policy specifies requirements, rules and conditions
that govern the access to and use (including transmission, processing,
and storage of data) of the service "{service name}" operated by
{community, agency, or infrastructure name} for the purpose of {describe
the stated goals and policies governing the intended use}.

## Requirements on the user management

Please specify any requirements on the user management of the Virtual
Organisation the service is registered with:

{specific requirements on user management}

## Requirements on attributes

In Addition to the common Infrastructure Attribute Profiles (IAP), the
Service requires the following attributes and/or entitlement values for
the purpose of authorisation:

  ----------------------- ----------------------- -----------------------
  **Attribute/Claim       **Value(s) - if         **Comments**
  Name**                  applicable**           

  {attribute 1}           {value 1}               {comment 1}

                                                 

                                                 
  ----------------------- ----------------------- -----------------------

## Additional Requirements or Restrictions

If there are any licensing or legal restrictions on the use of the
Service, please specify them here:

-   {restriction 1}

-   {restriction 2}

-   **...**

**Contact Data**

The administrative contact for this Service is: {email address for the
service operator}

The security contact for this Service is: {email address for the
security contact}

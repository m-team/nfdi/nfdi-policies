---
documentclass: article
title: Service Acceptable Use Policy Template
colorlinks: true
fontsize: 11pt
geometry: margin=20mm
papersize: a4
numbersections: true
header-includes:
    - # font
    - \renewcommand{\familydefault}{\sfdefault}

    - # linespacing
    - \usepackage{setspace}
    - \linespread{1.0}
    - \pagestyle{empty}

    - # footer
    - \pagenumbering{gobble}
    - \usepackage[markcase=noupper]{scrlayer-scrpage}
    - \cfoot*{\scriptsize\textcolor{gray}{{© Owned by the authors and made available under license{:} \href{https://creativecommons.org/licenses/by-nc-sa/4.0/}{https://creativecommons.org/licenses/by-nc-sa/4.0/} \\ Other Sources / Attribution / Acknowledgements{:} "The AARC Policy on the Processing of Personal Data", under CC BY-NC-SA 4.0 }}}

    - # logo on top right of first page
    - \usepackage{graphicx}
    - \graphicspath{{images/}}
    - \ohead*{\vspace{-18mm}\includegraphics[width=68.8mm,height=24.6mm]{nfdi-logo.png}}
    - \ohead{}
---
<!-- Add line numbers with: -->
<!-- - \usepackage{lineno} -->
<!-- - \linenumbers -->

<!-- Compile with:
pandoc <filename.md> -o <filename.pdf>
pandoc <filename.md> -o <filename.docx> --reference-doc style.docx
pandoc <filename.md> -o <filename.odt>  ## missing header and footer!
-->

(Version 0.9.4, August 19^th^, 2024)


The following defines the Acceptable Use Policy (AUP) for the Service \{service name\}:
Every user of your Service will see this AUP
and has to accept it before being able to use this Service. The general NFDI-AAI
policies, the AUP of the respective Community AAI and - if it exists - the VO AUP apply.
On top, you may define specific
conditions for your Service (e.g. specific purpose of using resources), or
provide additional information to your users (e.g. specific contacts).

This policy is effective from {insert date}.

# Acceptable Use Policy and Conditions of Use

This Acceptable Use Policy and Conditions of Use ("AUP") defines the
rules and conditions that apply to all users accessing the Service "\{service name\}".

- The NFDI-AAI Policies[^1] apply.
- The Community AAI Acceptable Use Policy[^2] applies.
- The VO Acceptable Use Policies of VOs that the Service supports (if available) apply.

You may add further specific conditions for your Service. These additions must
not conflict with any of the obovementioned AAI Policies and
AUPs.


# Contact

The administrative and security contact for this Service and AUP is: \{email
addresses of service contacts\}.

You may add further contacts (e.g. your support desk) that might help your
users.

[^1]: <https://doc.nfdi-aai.de/policies>
[^2]: <https://doc.nfdi-aai.de/policies/05-CAA_AUP.pdf>

---
documentclass: article
title: Top Level Infrastructure Policy
colorlinks: true
fontsize: 11pt
geometry: margin=20mm
papersize: a4
numbersections: true
header-includes:
    - # font
    - \renewcommand{\familydefault}{\sfdefault}

    - # linespacing
    - \usepackage{setspace}
    - \linespread{1.0}
    - \pagestyle{empty}

    - # footer
    - \pagenumbering{gobble}
    - \usepackage[markcase=noupper]{scrlayer-scrpage}
    - \cfoot*{\scriptsize\textcolor{gray}{{© Owned by the authors and made available under license{:} \href{https://creativecommons.org/licenses/by-nc-sa/4.0/}{https://creativecommons.org/licenses/by-nc-sa/4.0/} \\ Other Sources / Attribution / Acknowledgements{:} "The AARC Policy on the Processing of Personal Data", under CC BY-NC-SA 4.0 }}}

    - # logo on top right of first page
    - \usepackage{graphicx}
    - \graphicspath{{images/}}
    - \ohead*{\vspace{-18mm}\includegraphics[width=68.8mm,height=24.6mm]{nfdi-logo.png}}
    - \ohead{}
---
<!-- Add line numbers with: -->
<!-- - \usepackage{lineno} -->
<!-- - \linenumbers -->

<!-- Compile with:
pandoc <filename.md> -o <filename.pdf>
pandoc <filename.md> -o <filename.docx> --reference-doc style.docx
pandoc <filename.md> -o <filename.odt>  ## missing header and footer!
-->

(Version 0.9.4, August 19^th^, 2024)


# INTRODUCTION AND DEFINITIONS

To fulfil its mission, it is necessary for the National Research Data
Infrastructure (NFDI) - hereinafter referred to as *the
Infrastructure* - to protect its assets and those of the NFDI consortia.
This document presents the policy regulating those activities of
*Participants* related to the security and availability of the
Authentication and Authorization Infrastructure operated by the NFDI
(NFDI-AAI) governed by this *policy*.

Version 0.9, this *policy* is effective from October 1^st^, 2023.

This *policy* is one of a set of documents that together define the
NFDI-AAI Policies \[1\]. This individual document must be considered in
conjunction with all the policy documents in the set.

## Definitions

The terms below, when italicised in this document, are to be interpreted
in accordance to their following definitions:

- The term *Infrastructure* means the entirety of the natural and
    legal persons, organisations, hardware, software, networks,
    facilities, etc. within the National Research Data Infrastructure
    and its consortia that are required to develop, test, deliver,
    monitor, control, secure or support *Services*.

- An *Identity Provider (IdP)* is any system that creates, maintains,
    and manages identity information for *Users* while offering
    authentication functionality to relying *Services* and *SP-IdP
    proxy* within the *Infrastructure*.

- The *Infrastructure Management* is the collection of the various
    boards, committees, groups and individuals mandated to oversee and
    control the *Infrastructure*.

- The *Infrastructure Security Contact* is the collection of various
    individuals and groups mandated by the *Infrastructure Management*
    to lead and coordinate the operational security capability of the
    *Infrastructure*.

- The *NFDI-AAI* is the Authentication and Authorization
    Infrastructure operated by (or on behalf of) the NFDI.

- A *Participant* is any entity providing, using, managing, operating,
    supporting or coordinating one or more *Service(s)*.

- *Policy* is interpreted to include rules, responsibilities and
    procedures. These are specified in this document together with all
    those in other documents which are required to exist by stipulations
    in this document.

- A *Role* is a property that a *Participant* has. It comprises a set
    of rights and responsibilities within the *Infrastructure* and
    determines the *Participant*'s abilities to use and/or manage a
    *Service*, *Virtual Organisation* or any other part of the
    *Infrastructure*. A *Participant* can hold different *Roles* in
    different contexts. *Roles* are defined by the *Policies*.

- A *Service* is any ICT system or application accessible by *Users*
    of the *Infrastructure*.

- A *Service Provider (SP)* is any entity offering *Services*.

- A *Service Provider to Identity Provider proxy (SP-IdP proxy)* \[4\]
    -- introduced in the AARC Blueprint Architecture \[5\] used to
    implement federated access management solutions for international
    research collaborations -- is a single component that negotiates
    between *Services* and *IdPs*, thereby shielding the community
    *Services* from the heterogeneity of global identity federations.

- A *User* is an individual who has been given authority to access and
    use *Services*.

- A *Virtual Organisation (VO)* is a group of one or more *Users*, not
    necessarily bound to a single institution, organised with a common
    purpose, jointly granted access to one or more *Services*. In many
    cases, a Virtual Organisation unites the users of a specific
    research community represented by one or more of the NFDI consortia.
    It may serve as an entity which acts as the interface between the
    individual *Users* and an *Infrastructure*. In general, the members
    of the *Virtual Organisation* will not need to separately negotiate
    access with *Service Providers*. A *User* can be a member of
    multiple *Virtual Organisations*.

- The *Virtual Organisation Management* is the collection of various
    individuals and groups mandated to oversee and control a *Virtual
    Organisation*.

- The *Virtual Organisation Supervisor* is the collection of various
    individuals and groups delegated from the *Infrastructure
    Management* to oversee and approve requests for registration and
    deregistration of *Virtual Organisations*.

Other infrastructures, frameworks or standards mentioned in this
document are described below:

- *Sirtfi* \[2\] is a trust framework aiming to enable coordination of
    security incident response across federated organisations by
    describing practices and attributes that identify an organisation as
    being capable of effectively participating in incident response --
    i.e. is *Sirtfi* compliant.

- The *REFEDS Assurance Framework (RAF)* \[3\] defines requirements
    for identity assurance, as well as two assurance profiles based on
    these requirements. Identity Assurance conveys the level of
    confidence that an identity belongs to the expected *User*; this
    includes identity vetting, multi factor authentication and the
    security provisions of the *Identity Provider* among other factors.
    The aim is to provide a basis for *SPs* to make decisions on how
    much to trust assertions made by *IdPs*, and manage risks related to
    access control to their *Services*.

- The *REFEDS Data Protection Code of Conduct Entity Category* \[7\]
    defines a set of GDPR-based data protection rules and best practices
    that Service Provider Organisations can commit to when they want to
    receive End Users' Attributes from Home Organisations or their Agent
    for enabling the End Users to access their Services.

## Objectives

This *policy* gives authority for actions as defined in this *policy*,
which may be carried out by designated individuals and organisations,
and places responsibilities on all *Participants*.

## Scope

This *policy* applies to all *Participants*. This *policy* augments
local *Service* policies by setting out additional *Infrastructure*
specific requirements.

## Approval and Maintenance

This *policy* is approved by the *Infrastructure Management* and thereby
endorsed and adopted by the *Infrastructure* as a whole. This *policy*
will be maintained and revised by a collection of various individuals
and groups appointed by the *Infrastructure Management* as required and
resubmitted for formal approval and adoption whenever significant
changes are needed. The most recently approved version of this document
is available at a location specific to the *Infrastructure* \[1\].

## Additional Policy Documents

Additional policy documents required for a proper implementation of this
policy may be found at a location specific to the *Infrastructure*
\[1\], and are listed below:

- Community-AAI Acceptable Use Policy (CAAI AUP)

- Virtual Organisation Membership Management Policy (VOMMP)

- Policy on the Processing of Personal Data (PPPD)

- Security Incident Response Procedure (SIRP)

- Infrastructure Attribute Profiles (IAP)

These *Infrastructure*-defined policies must be complemented by local
policies. The following list of policies may need to be specified by
*Participants*, depending on which *Infrastructure* components (see
section 2.2) are operated.

- Service Privacy Policy (SPP)

- Virtual Organisation Acceptable Use Policy (VO AUP)

- SP-IdP proxy Privacy Policy (Proxy PP)

- Service Acceptable Use Policy (SAUP) -- optional

- Service Access Policy (SAP) -- optional

- Virtual Organisation Privacy Policy (VO PP) -- optional (unless
    *VOs* process or control personal data)

Templates are provided for Acceptable Use Policies, Service Access
Policies and Privacy Policies \[1\].

Figure 1 gives an overview of the NFDI-AAI policies, specifying, for
each policy, the *Participant* that defines (or should define) the
policy, and the *Participant*(s) that must abide by the policy.

![](images/tabelle_policy-framework.png){width="6.692716535433071in"}

Figure 1: Overview of NFDI-AAI policies, with respect to the
Participants involved in defining them, as well as abiding by the
specified policies.

# ROLES AND RESPONSIBILITIES

This section defines the roles and responsibilities of *Participants*.

## Roles

- Infrastructure Management

- Infrastructure Security Contact

- User

- Virtual Organisation Management

- Virtual Organisation Supervisor

- Identity Provider

- Service Provider

- SP-IdP proxy

## The Infrastructure

### Infrastructure Management

The *Infrastructure Management* provides, through the adoption of this
*policy* and through its representations on the various management
bodies of the *Infrastructure*, the overall authority for the decisions
and actions resulting from this *policy* including procedures for the
resolution of disputes.

The *Infrastructure Management* provides the capabilities for meeting
its responsibilities with respect to this *policy*. The *Infrastructure
Management* is responsible for taking all necessary actions to ensure
compliance of its *Participants* and can represent them towards third
parties with respect to this *policy*.

The *Infrastructure Management* must maintain a registry of Privacy
Policies of *Services* to which personal data may be released.

The *Infrastructure Management* must appoint an *Infrastructure Security
Contact* who leads and coordinates the operational security capability
of the *Infrastructure*.

### Infrastructure Security Contact

The *Infrastructure Security Contact* must support the requirements of
the *Sirtfi* framework \[2\] on behalf of the *Infrastructure*.

The *Infrastructure Security Contact* must, in consultation with the
*Infrastructure Management* and other appropriate persons, require
actions by *Participants* as are deemed necessary to protect the
*Infrastructure* from or contain the spread of IT security incidents.

The *Infrastructure Security Contact* also handles requests for
exceptions to this *policy* as described in section 5. The
*Infrastructure Security Contact* is responsible for establishing
periodical tests of a communications flow to all Security Contact Points
for use in security incidents.

The *Infrastructure Security Contact* is security@nfdi.de.

## Identity Provider

*Identity Providers* must support / abide by:

- *Sirtfi* \[2\]

> *Identity Providers* must comply with the *Sirtfi* framework \[2\].
> *Identity Providers* must designate a Security contact point (person
> or team) that is willing and able to collaborate with affected
> *Participants* in the management of security incidents and make it
> known to the *Infrastructure Security Contact*.

- *REFEDS Assurance Framework (RAF)* \[3\]

> *Identity Providers* must abide by the provisions on acceptable
> authentication assurance in section 3. *Identity Providers* must
> support the *REFEDS Assurance Framework (RAF)* \[3\] to describe the
> levels of assurance of their *Users*.

- Infrastructure Attribute Profiles (IAP)

> *Identity Providers* must support the *Federation Attribute Profile*
> specified in the IAP.

- Security Incident Response Procedure (SIRP)

- Service Access Policies (SAP)

> The use of each *Service* may require the delivery of additional
> *User* attributes, such as "entitlements" for authorising the use of
> specific *Services*. These attributes can be found in the Service
> Access Policy of the *Service*. *Identity Providers* must ensure the
> ability to send these additional attributes, and the attributes must
> be delivered only in agreement with *Service Providers*.

*Identity Providers* must acknowledge that their mere participation in
the *Infrastructure* does not entitle their *Users* to use all the
*Services* offered in the *NFDI, its consortia or connected
infrastructures as e.g. the European Open Science Cloud, EOSC*. The use
of the individual *Services* may require bilateral agreements between
the organisations of the *Identity Providers* and those of the *Service
Providers*.

*Identity Providers* must provide correct information about their
*Users*. Correctness is defined by the *REFEDS Assurance Framework*
\[3\]. For additional attributes, the requirements of the respective
*Service Provider* apply, as specified in their Service Access Policy
(SAP).

*Identity Providers* must inform *Users* about the data to be
transmitted by the *Identity Provider* to the *Service Provider*.

## Virtual Organisation Management

*Virtual Organisations* must support / abide by:

- *REFEDS Assurance Framework (RAF)* \[3\]

> The *Virtual Organisation Management* must abide by the provisions on
> acceptable authentication assurance in section 3. The *Virtual
> Organisation Management* must support the *REFEDS Assurance Framework
> (RAF)* \[3\] to describe the levels of assurance of *Users*.

- Security Incident Response Procedure (SIRP)

> The *Virtual Organisation Management* must designate a Security
> contact point (person or team) that is willing and able to collaborate
> with affected *Participants* in the management of security incidents,
> in compliance with the Security Incident Response Procedure (SIRP).

- Virtual Organisation Membership Management Policy (VOMMP)

> The *Virtual Organisation Management* must abide by the Virtual
> Organisation Membership Management Policy. Exceptions to this must be
> handled as in section 5.

- Policy on the Processing of Personal Data (PPPD)

> *Virtual Organisations* that operate their own Registry (containing
> membership data of the *VO*) must comply with the Policy on the
> Processing of Personal Data. The Registry must also be operated in a
> manner compliant with *Sirtfi* \[2\]. A *Virtual Organisation* may
> delegate the operation of its Registry to the *SP-IdP proxy*.

*Virtual Organisations* must define:

- Virtual Organisation Acceptable Use Policy (VO AUP)

> The *Virtual Organisation* must define, and provide to the
> *Infrastructure*, a Virtual Organisation Acceptable Use Policy (VO
> AUP), and ensure that its members are aware of and agree to abide by
> this AUP. The *Virtual Organisation Management* must ensure that only
> individuals who have agreed to abide by the Virtual Organisation AUP
> and have been presented with the VO Privacy Policy or SP-IdP proxy
> Privacy Policy are registered as members of the *Virtual
> Organisation*. The acceptance of the AUP must be recorded for audit
> trail and repeated at least once a year, or upon material changes to
> its content.

- Virtual Organisation Privacy Policy (VO PP)

> *Virtual Organisations* that process or control personal data must
> define a Virtual Organisation Privacy Policy (VO PP).

The *Virtual Organisation Management* is responsible for promptly
investigating reports of *Users* failing to comply with the policies and
for taking appropriate action to limit the risk to the *Infrastructure*
and ensure compliance in the future.

The *Virtual Organisation Management* is responsible for obtaining
support for their *Virtual Organisation* from *Services* (e.g. through
MoUs or other types of agreements), and ensuring that all the *Service*
requirements placed upon the *Users* are met, including identity
assurance.

## SP-IdP Proxy

The *SP-IdP proxy* must support / abide by:

- *Sirtfi* \[2\]

> The *SP-IdP proxy* must designate a Security contact point (person or
> team) that is willing and able to collaborate with affected
> *Participants* in the management of security incidents and to take
> prompt action as necessary to safeguard the *SP-IdP proxy* during an
> incident and make it known to the *Infrastructure Security Contact*.
> The Security contact must also support the requirements of the
> *Sirtfi* framework \[2\] on behalf of the *SP-IdP proxy*.

- *REFEDS Assurance Framework (RAF)* \[3\]

> The *SP-IdP proxy* must abide by the provisions on acceptable
> authentication assurance in section 3.

- Infrastructure Attribute Profiles (IAP)

> The *SP-IdP proxy* must be able to process the attributes of the
> *Federation Attribute Profile* (SP part) and support the *Core and
> Extended Community Attribute Profiles* (IdP part) specified in the
> IAP.

- *REFEDS Data Protection Code of Conduct \[7\]*\
    The SP-IdP proxy must abide by the *REFEDS Data Protection Code of
    Conduct \[7\]*

- Security Incident Response Procedure (SIRP)

- Policy on the Processing of Personal Data (PPPD)

The *SP-IdP proxy* must define an SP-IdP proxy Privacy Policy (Proxy
PP).

The *SP-IdP proxy* must forward entitlements from the home *IdPs* to
*SPs*.

An *SP-IdP proxy* acting as a Community AAI must define a Community AAI
Acceptable Use Policy (CAAI AUP).

## Service Provider

*Service Providers* must support / abide by:

- *Sirtfi* \[2\]

> *Service Providers* must comply with the *Sirtfi* framework \[2\]. The
> *Service Provider* must designate a Security contact point (person or
> team) that is willing and able to collaborate with affected
> *Participants* in the management of security incidents and to take
> prompt action as necessary to safeguard *Services* during an incident
> and make it known to the *Infrastructure Security Contact*.
>
> *Service Providers* must deploy effective security controls to protect
> the confidentiality, integrity and availability of their *Services*.

- *REFEDS Assurance Framework (RAF)* \[3\]

> For *Services* requiring authentication of entities, the *Service
> Providers* must abide by the provisions on acceptable authentication
> assurance in section 3.

- Infrastructure Attribute Profiles (IAP)

> *Service Providers* should limit their data requirements to the bundle
> of attributes defined in the *Core and Extended Community Attribute
> Profiles* specification of the IAP, but may negotiate for additional
> data as required, via *Service Access Policies*.

- *REFEDS Data Protection Code of Conduct* \[7\]\
    *Service Providers* must abide by the *REFEDS Data Protection Code
    of Conduct* \[7\]

- Security Incident Response Procedure (SIRP)

- Policy on the Processing of Personal Data (PPPD)

> For *Services* receiving personal data, *Service Providers* must
> comply with the Policy on the Processing of Personal Data (PPPD).

*Service Providers* must define, for each *Service*:

- Service Privacy Policy (SPP)

> For each *Service* that processes or controls personal data, a Service
> Privacy Policy (SPP) must be defined and shared with the
> *Infrastructure Management*, and presented to *Users* upon first
> access to the *Service*.
>
> *Service Providers* are responsible for recording sufficient
> information such that personal data can be cleansed after the
> retention period is reached. The recorded information and the
> retention period must be specified in the local Service Privacy Policy
> (SPP).

*Service Providers* may define, for each *Service*:

- Service Acceptable Use Policy (SAUP)

> For each *Service* provided, *Service Providers* may specify a Service
> Acceptable Use Policy (SAUP) if the AUPs in force in the
> *Infrastructure* are not acceptable, and ensure that its *Users* are
> aware of and agree to abide by this AUP.

- Service Access Policy (SAP)

> For each *Service* provided, *Service Providers* may specify a Service
> Access Policy (SAP), where supported assurance profiles can be
> defined, as well as any additional attributes that are required for
> providing the *Service*.

*Service Providers* acknowledge that participating in the
*Infrastructure* and allowing related inbound and outbound network
traffic increases their IT security risk. *Service Providers* are
responsible for accepting or mitigating this risk.

## User

*Users* must abide by:

- Virtual Organisation Acceptable Use Policy (VO AUP)

> *Users* must accept and agree to abide by the Virtual Organisation
> Acceptable Use Policy (VO AUP) when they register or renew their
> registration with a *Virtual Organisation*, as well as upon changes in
> this policy.

- Community AAI Acceptable Use Policy (CAAI AUP)

> *Users* must accept and agree to abide by the Community AAI
> Acceptable Use Policy (CAAI AUP) when they register or renew their
> registration with a *Community AAI*, as well as upon changes in
> this policy.

- Service Acceptable Use Policy (SAUP)

> *Users* must accept and agree to abide by the Service Acceptable Use
> Policy (SAUP) of each *Service* they use.

*Users* that have been authorised to use a *Service* by virtue of their
membership in a *Virtual Organisation* must use the *Service* only in
pursuit of the legitimate purposes of their *Virtual Organisation*.

*Users* must not attempt to circumvent any restrictions on access to
*Services*. *Users* must show responsibility, consideration and respect
towards other *Participants* in the demands they place on the
*Services*.

*Users* may be held responsible for all actions taken using their
credentials, whether carried out personally or not. No intentional
sharing of *User* credentials is permitted.

# ACCEPTABLE AUTHENTICATION ASSURANCE

In line with the *REFEDS Assurance Framework (RAF)* \[3\], assurance
information is expressed by asserting individual assurance components,
as well as composite assurance profiles. Assurance profiles can be
composed by information derived from several sources in the
*Infrastructure*: *Identity Providers*, *Virtual Organisations*, *SP-IdP
proxy* (see \[6\] for AARC policy guidelines to effectively implement
authentication assurance).

In the HIFIS *Infrastructure*, there is no minimal assurance level.
Different assurance levels are supported explicitly. *Identity
Providers* should support *RAF* \[3\]. *Services* should filter *Users*
by their assurance level and the *Virtual Organisation Management* can
(under specific circumstances) take measures to elevate the assurance of
*Users* (e.g. by checking a person's passport according to defined
procedures).

Assurance information will be propagated with the *User*'s
authentication token for relying *Services* to include in authorisation
decisions. Only *Users* conforming to one of the approved authentication
assurance profiles shall be granted access to the *Infrastructure*.

# PHYSICAL AND NETWORK SECURITY

All the requirements for the physical security of *Services* are
expected to be adequately covered by each *Service Provider*'s local
security policies and practices. The technical details of such
additional requirements are contained in the procedures for operating
and approving such *Services*.

Networking security of *Services* is expected to be adequately covered
by each *Service Provider*'s local security policies and practices.

# EXCEPTIONS TO COMPLIANCE

Wherever possible, *Infrastructure* policies and procedures are designed
to apply uniformly to all *Participants*. If this is not possible, for
example due to legal or contractual obligations, exceptions may be made.
Such exceptions should be time-limited and must be documented and
authorised by the *Infrastructure Security Contact* and, if required,
approved at the appropriate level of the *Infrastructure Management*.

In exceptional circumstances it may be necessary for *Participants* to
take emergency action in response to some unforeseen situation which may
violate some aspect of this *policy* for the greater good of pursuing or
preserving legitimate *Infrastructure* objectives. If such a policy
violation is necessary, the exception should be minimised, documented,
time-limited and authorised at the highest level of the *Infrastructure
Management* commensurate with taking the emergency action promptly, and
the details notified to the *Infrastructure Security Contact* at the
earliest opportunity.

# SANCTIONS

*Service Providers* that fail to comply with this *policy* in respect of
a *Service* they are operating may lose the right to have their
*Services* recognised by the *Infrastructure* until compliance has been
satisfactorily demonstrated again.

*Identity Providers* who fail to comply with this *policy* may lose
their right of access to the *Infrastructure* until compliance has been
satisfactorily demonstrated again.

*Virtual Organisations* who fail to comply with this *policy* may lose
their right of access to and collaboration with the *Infrastructure*
until compliance has been satisfactorily demonstrated again.

*Users* who fail to comply with this *policy* may lose their right of
access to the *Infrastructure*, and may have their activities reported
to their *Virtual Organisation* or their home organisation.

Any activities thought to be illegal may be reported to appropriate law
enforcement agencies.

# FEES

There are no charges for using the *Infrastructure*. The billing of paid
*Services* does not fall under the jurisdiction of the *Infrastructure*
and may be subject to bilateral agreements between the individual
*Participants*.

# SALVATORY CLAUSE

Should any provision of this *policy* be or become invalid, this shall
not affect the validity of the remaining provisions or the agreement as
a whole. The provision shall be replaced retroactively by a provision
which is legally permissible and whose content comes close to the
original provision. The same applies to existing loopholes.

This *policy* is subject to German law and German jurisdiction.

# REFERENCES

\[1\] NFDI-AAI Policies -- [[https://doc.nfdi-aai.de/policies]{.underline}](https://doc.nfdi-aai.de/policies)

\[2\] Security Incident Response Trust Framework for Federated Identity
(Sirtfi) -
[[https://refeds.org/sirtfi]{.underline}](https://refeds.org/sirtfi)

\[3\] REFEDS Assurance Framework (RAF) -
[[https://refeds.org/assurance]{.underline}](https://refeds.org/assurance)

\[4\] Scalable Negotiator for a Community Trust Framework in Federated
Infrastructures (Snctfi) -
[[https://www.igtf.net/snctfi/]{.underline}](https://www.igtf.net/snctfi/)

\[5\] AARC Blueprint Architecture -
[[https://aarc-project.eu/architecture/]{.underline}](https://aarc-project.eu/architecture/)

\[6\] AARC policy guidelines -
[[https://aarc-project.eu/guidelines/#policy]{.underline}](https://aarc-project.eu/guidelines/#policy)

\[7\] REFEDS Data Protection Code of Conduct -
[[https://refeds.org/category/code-of-conduct]{.underline}](https://refeds.org/category/code-of-conduct)

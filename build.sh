#!/bin/bash

set -e

POLICIES="\
    00_Top-Level-Policy\
    01_CAAI-VOMMP\
    02_SIRP\
    03_PPPD\
    04_IAP\
    "

TEMPLATES="\
    05-CAAI_AUP\
    05-CAAI_AUP_DE\
    05-VO_AUP_template\
    05-VO_AUP_template_DE\
    05a-Service_AUP_template\
    05a-Service_AUP_template_DE\
    06_SAP_template\
    07_Privacy_Policy_Template\
    07_Privacy_Policy_Template_DE\
    91_VO-lifecycle\
    "

TARGET="BUILD"
TARGET_LIST=""

while [ $# -gt 0 ]; do
    case "$1" in
    clean)      TARGET="CLEAN"                                          ;;
    *)          TARGET_LIST="${TARGET_LIST} $1"                         ;;
    esac
    shift
done

[ "x$TARGET" == "xBUILD" ] && {
    [ -z ${TARGET_LIST} ] || {
        for a in ${TARGET_LIST} ; do
            i=`echo $a | awk -F. '{ print $1 }'`
            echo -n "$i"
            pandoc $i.md -o $i.pdf --lua-filter=utils/list-table.lua
            pandoc $i.md -o $i.docx --reference-doc style.docx
            echo "...done"
        done
    }
    [ -z ${TARGET_LIST} ] && {
        for a in $POLICIES $TEMPLATES ; do
            i=`echo $a | awk -F. '{ print $1 }'`
            echo -n "$i"
            pandoc $i.md -o $i.pdf --lua-filter=utils/list-table.lua
            echo "...done"
        done

        for a in $TEMPLATES; do
            i=`echo $a | awk -F. '{ print $1 }'`
            echo -n "$i"
            pandoc $i.md -o $i.docx --reference-doc style.docx
            echo "...done"
        done
    }
}

[ "x$TARGET" == "xCLEAN" ] && {
    for a in $POLICIES $TEMPLATES ; do
        i=`echo $a | awk -F. '{ print $1 }'`
        echo -n "$i"
        rm -f $i.pdf $i.docx
        echo "...done"
    done
}

exit 0

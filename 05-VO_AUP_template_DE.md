---
documentclass: article
title: VO Acceptable Use Policy Template (DE)
colorlinks: true
fontsize: 11pt
geometry: margin=20mm
papersize: a4
numbersections: true
header-includes:
    - # font
    - \renewcommand{\familydefault}{\sfdefault}

    - # linespacing
    - \usepackage{setspace}
    - \linespread{1.0}
    - \pagestyle{empty}

    - # footer
    - \pagenumbering{gobble}
    - \usepackage[markcase=noupper]{scrlayer-scrpage}
    - \cfoot*{\scriptsize\textcolor{gray}{{© Owned by the authors and made available under license{:} \href{https://creativecommons.org/licenses/by-nc-sa/4.0/}{https://creativecommons.org/licenses/by-nc-sa/4.0/} \\ Other Sources / Attribution / Acknowledgements{:} "The AARC Policy on the Processing of Personal Data", under CC BY-NC-SA 4.0 }}}

    - # logo on top right of first page
    - \usepackage{graphicx}
    - \graphicspath{{images/}}
    - \ohead*{\vspace{-18mm}\includegraphics[width=68.8mm,height=24.6mm]{nfdi-logo.png}}
    - \ohead{}
---
<!-- Add line numbers with: -->
<!-- - \usepackage{lineno} -->
<!-- - \linenumbers -->

<!-- Compile with:
pandoc <filename.md> -o <filename.pdf>
pandoc <filename.md> -o <filename.docx> --reference-doc style.docx
pandoc <filename.md> -o <filename.odt>  ## missing header and footer!
-->

(Version 0.9.4, August 19^th^, 2024)

Im folgenden werden die Richtlinien zur zulässigen Nutzung (Acceptable Use Policy, "AUP")
für Ihre Virtuelle Organisation ("VO") dargelegt:
Diese AUP werden allen Nutzenden, die Ihrer VO beitreten, angezeigt und müssen von diesen
akzeptiert werden, bevor sie der VO beitreten können.
Es gelten die allgemeinen Policies der NFDI-AAI sowie die AUP der jeweiligen Community AAI.
Ergänzend können Sie weitere, spezifische Regeln für Ihr VO festlegen (z.B. spezifische
Verwendungszwecke bestimmter Ressourcen) oder zusätzliche Informationen für die
Nutzenden Ihrer VO bereitstellen, wie z.B. spezfische Kontaktadressen.

---

Diese Richtlinien gelten ab dem {Datum einfügen}.

# Nutzungsbedingungen und Richtlinien zur zulässigen Nutzung (Acceptable Use Policy, "AUP")

Diese Bedingungen und Richtlinien legen die Regeln und Bedingungen dar,
die für alle Nutzenden gelten, die der Virtuellen Organisation (VO)
"\{vollständiger Name der VO\}" beitreten.

- Es gelten die allgemeinen NFDI-AAI Policies[^1].
- Es gelten die Nutzungsbedingungen und Richtlinien zur zulässigen Nutzung (Acceptable Use Policy, "AUP")
  der betreffenden Community AAI[^2].

Es können weitere spezifische Bedingungen für Ihre VO festgelegt werden.
Diese Ergänzungen dürfen nicht im Widerspruch zu den oben genannten Policies und AUPs stehen.

# Konatkt

Administrativer Kontakt: \{E-Mail-Adresse(n) der zuständigen VO-Manager\}

Kontakt für Sicherheitsfragen: \{E-Mail-Adresse der für Sicherheitsfragen zuständigen Stelle
der betreffenden VO}}

Sie können weitere Kontaktangaben ergänzen, die für Ihre Nutzenden relevant sind, z.B. ein Helpdesk.

[^1]: <https://doc.nfdi-aai.de/policies>
[^2]: <https://doc.nfdi-aai.de/policies/05-CAA_AUP_DE.pdf>

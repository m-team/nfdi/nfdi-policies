---
documentclass: article
title: VO Acceptable Use Policy Template
colorlinks: true
fontsize: 11pt
geometry: margin=20mm
papersize: a4
numbersections: true
header-includes:
    - # font
    - \renewcommand{\familydefault}{\sfdefault}

    - # linespacing
    - \usepackage{setspace}
    - \linespread{1.0}
    - \pagestyle{empty}

    - # footer
    - \pagenumbering{gobble}
    - \usepackage[markcase=noupper]{scrlayer-scrpage}
    - \cfoot*{\scriptsize\textcolor{gray}{{© Owned by the authors and made available under license{:} \href{https://creativecommons.org/licenses/by-nc-sa/4.0/}{https://creativecommons.org/licenses/by-nc-sa/4.0/} \\ Other Sources / Attribution / Acknowledgements{:} "The AARC Policy on the Processing of Personal Data", under CC BY-NC-SA 4.0 }}}

    - # logo on top right of first page
    - \usepackage{graphicx}
    - \graphicspath{{images/}}
    - \ohead*{\vspace{-18mm}\includegraphics[width=68.8mm,height=24.6mm]{nfdi-logo.png}}
    - \ohead{}
---
<!-- Add line numbers with: -->
<!-- - \usepackage{lineno} -->
<!-- - \linenumbers -->

<!-- Compile with:
pandoc <filename.md> -o <filename.pdf>
pandoc <filename.md> -o <filename.docx> --reference-doc style.docx
pandoc <filename.md> -o <filename.odt>  ## missing header and footer!
-->

(Version 0.9.4, August 19^th^, 2024)


The following defines the Acceptable Use Policy (AUP) for your Group
(Virtual Organisation, VO): Every user joining your VO will see this AUP
and has to accept it before being able to join this VO. The general NFDI-AAI
policies and the AUP of the respective Community AAI apply. On top, you may define specific
conditions for your VO (e.g. specific purpose of using resources), or
provide additional information to your users (e.g. specific contacts).

---

This policy is effective from {insert date}.

# Acceptable Use Policy and Conditions of Use

This Acceptable Use Policy and Conditions of Use ("AUP") defines the
rules and conditions that apply to all users joining the virtual
organisation (VO) "\{full VO name\}".

- The NFDI-AAI Policies[^1] apply.
- The Community AAI Acceptable Use Policy[^2] applies.

You may add further specific conditions for your VO. These additions must
not conflict with any of the obovementioned AAI Policies and
AUPs.


# Contact

The administrative and security contact for this VO and AUP is: \{email
addresses of all VO Managers\}.

You may add further contacts (e.g. your support desk) that might help your
users.

[^1]: <https://doc.nfdi-aai.de/policies>
[^2]: <https://doc.nfdi-aai.de/policies/05-CAA_AUP.pdf>

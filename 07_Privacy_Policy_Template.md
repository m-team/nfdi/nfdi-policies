---
documentclass: article
title: Privacy Statement Template
colorlinks: true
fontsize: 11pt
geometry: margin=20mm
papersize: a4
numbersections: true
header-includes:
 - # font
 - \renewcommand{\familydefault}{\sfdefault}

 - # linespacing
 - \usepackage{setspace}
 - \linespread{1.0}
 - \pagestyle{empty}

 - # footer
 - \pagenumbering{gobble}
 - \usepackage[markcase=noupper]{scrlayer-scrpage}
 - \cfoot*{\scriptsize\textcolor{gray}{Author{:} DFN-CERT Services GmbH}}

 - # logo on top right of first page
 - \usepackage{graphicx}
 - \graphicspath{{images/}}
 - \ohead*{\vspace{-18mm}\includegraphics[width=68.8mm,height=24.6mm]{nfdi-logo.png}}
 - \ohead{}
---
<!-- Add line numbers with: -->
<!-- - \usepackage{lineno} -->
<!-- - \linenumbers -->

<!-- Compile with:
pandoc <filename.md> -o <filename.pdf>
pandoc <filename.md> -o <filename.docx> --reference-doc style.docx
pandoc <filename.md> -o <filename.odt> ## missing header and footer!
-->

(Version 0.9.4, August 19^th^, 2024)

# Name of the Service

Please fill in a descriptive name to unambiguously identify the
service


# Description of the Service

A short, precise description of the service


# Data controller

\'Controller\' means the natural or legal person, public
authority, agency or other body which, alone or jointly with others,
determines the purposes and means of the processing of personal data.
Please name the controller (legal entity).


# Purpose of the processing of personal data

(Personal Data processed for the purpose and legal basis)

Please specify the purposes of the processing, for each purpose the legal
basis and the categories of personal data processed for this purpose. The
legal permission is always based in the starting point on Art. 6(1) or
Art. 9(1) GDPR.

**Special cases:**

- Where the processing is based on point (f) of Art. 6(1) GDPR, the
 legitimate interests pursued by the controller or by a third
 party are to be mentioned.
- If applicable: Whether the provision of personal data is a
 statutory or contractual requirement, or a requirement
 necessary to enter into a contract, as well as whether the data
 subject is obliged to provide the personal data and of the
 possible consequences of failure to provide such data.


# Third parties to whom personal data is disclosed

Please name external persons/legal entities to whom personal
data is disclosed.


# Personal data transfer to Third Country (where GDPR does not apply)

If not applicable: No statement required

If applicable: the fact that the controller intends to transfer personal
data to a third country or international organisation and the existence or
absence of an adequacy decision by the Commission, or in the case of
transfers referred to in Article 46 or 47, or the second subparagraph of
Article 49(1), reference to the appropriate or suitable safeguards and the
means by which to obtain a copy of them or where they have been made
available.


# Storage period

The period for which the personal data will be stored, or if that
is not possible, the criteria used to determine that period.


# Rights and how to exercise them

You have specific rights as a 'data subject' under Chapter III
(Articles 12-23) of Regulation (EU) 2016/679, in particular the right to
access your personal data, and, to rectify them, in case your
personal data are inaccurate or incomplete. Where applicable, you
have the right to erase your personal data, to restrict the processing
of your personal data, to object to the processing, and the right to
data portability, and the right not to be subject to a decision based
solely on automated processing, including profiling, which produces
legal effects concerning you or similarly significantly affects
you.

You can exercise your rights by contacting the Data Controller, or, in
case of conflict, the Data Protection Officer.


# Withdrawal of consent

*Only applicable where the processing is based on point (a) of
Article 6(1) or point (a) of Article 9(2):* the existence of the right
to withdraw consent at any time, without affecting the lawfulness of
processing based on consent before its withdrawal.

*Otherwise:* not applicable


# Data portability

Only applicable if the processing is based on consent pursuant
to point (a) of Article 6(1) or point (a) of Article 9(2) or on a
contract pursuant to point (b) of Article 6(1) and the processing is
carried out by automated means.

Otherwise: not applicable


# Right to lodge a complaint with a supervisory authority

Every data subject has the right to lodge a complaint with a supervisory
authority if the data subject considers that the processing of
personal data relating to him or her infringes the GDPR. The
supervisory authority responsible for the controller is: *Please
name the supervisory authority with the contact details*

# Data Controller's data protection officer or contact person

If applicable: Please name the data protection officer (DPO) with
contact details.

If not applicable: Please name a contact person for data protection
issues.

# Existence of automated decision-making

If automated decision-making, including profiling, referred to in
Article 22(1) and (4) of the GDPR is used in this processing activity,
and, at least in these cases, meaningful information about the logic
involved, as well as the significance and the envisaged consequences
of such processing for the data subject.

If not: No statement required


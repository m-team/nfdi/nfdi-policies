---
documentclass: article
title: CAAI Acceptable Use Policy
colorlinks: true
fontsize: 11pt
geometry: margin=20mm
papersize: a4
numbersections: true
header-includes:
    - # font
    - \renewcommand{\familydefault}{\sfdefault}

    - # linespacing
    - \usepackage{setspace}
    - \linespread{1.0}
    - \pagestyle{empty}

    - # footer
    - \pagenumbering{gobble}
    - \usepackage[markcase=noupper]{scrlayer-scrpage}
    - \cfoot*{\scriptsize\textcolor{gray}{{© Owned by the authors and made available under license{:} \href{https://creativecommons.org/licenses/by-nc-sa/4.0/}{https://creativecommons.org/licenses/by-nc-sa/4.0/} \\ Other Sources / Attribution / Acknowledgements{:} "The AARC Policy on the Processing of Personal Data", under CC BY-NC-SA 4.0 }}}

    - # logo on top right of first page
    - \usepackage{graphicx}
    - \graphicspath{{images/}}
    - \ohead*{\vspace{-18mm}\includegraphics[width=68.8mm,height=24.6mm]{nfdi-logo.png}}
    - \ohead{}
---
<!-- Add line numbers with: -->
<!-- - \usepackage{lineno} -->
<!-- - \linenumbers -->

<!-- Compile with:
pandoc <filename.md> -o <filename.pdf>
pandoc <filename.md> -o <filename.docx> --reference-doc style.docx
pandoc <filename.md> -o <filename.odt>  ## missing header and footer!
-->

(Version 0.9.4, August 19^th^, 2024)

# Acceptable Use Policy and Conditions of Use

This Acceptable Use Policy and Conditions of Use ("AUP") defines the rules
and conditions that govern your access to and use (including transmission,
processing, and storage of data) of the resources and services
("Services") as granted by the NFDI infrastructure for the purpose of
research in the context of an NFDI consortium.

By registering as a user you declare that you have read, understood and
will abide by the following conditions of use:

1. You shall only use the Services in a manner consistent with the
   purposes and limitations described above; you shall show consideration
   towards other users including by not causing harm to the Services; you
   have an obligation to collaborate in the resolution of issues arising
   from your use of the Services.
2. You shall only use the Services for lawful purposes and not breach,
   attempt to breach, nor circumvent administrative or security controls.
3. You shall respect intellectual property and confidentiality agreements.
4. You shall protect your access credentials (e.g. passwords, private keys
   or multi-factor tokens); no intentional sharing is permitted.
5. You shall keep your registered information correct and up to date.
6. You shall promptly report known or suspected security breaches,
   credential compromise, or misuse to the security contact stated below;
   and report any compromised credentials to the relevant issuing
   authorities.
7. Reliance on the Services shall only be to the extent specified by any
   applicable service level agreements listed below. Use without such
   agreements is at your own risk.
8. Your personal data will be processed in accordance with the privacy
   statements referenced below
9. Your use of the Services may be restricted or suspended, for
   administrative, operational, or security reasons, without prior notice
   and without compensation.
10. If you violate these rules, you may be liable for the consequences,
    which may include your account being suspended and a report being made
    to your home organisation or to law enforcement.

11. You shall provide appropriate acknowledgement of support or citation
    for your use of the resources/services provided as required by the
    body or bodies granting you access.

The administrative contact for this AUP is: {email address for the
community, agency, or infrastructure name}

The security contact for this AUP is: {email address for the community,
agency, or infrastructure security contact}

The privacy statements (e.g. Privacy Notices) are located at: {URL}

Applicable service level agreements are located at: \<URLs\>

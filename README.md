# NFDI AAI Policies and Templates

## Policies
<!-- https://example.com/<namespace>/<project>/-/jobs/artifacts/<ref>/raw/<path_to_file>?job=<job_name> -->
<!-- https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/xxx?job=build-docs -->

- Top Level Policy: [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/00_Top-Level-Policy.pdf?job=build-docs)
- VO Membership Management Policy (VOMMP): [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/01_CAAI-VOMMP.pdf?job=build-docs)
- Security Incident Response Proecedure (SIRP): [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/02_SIRP.pdf?job=build-docs)
- Policy on Processing of Personal Data (PPPD): [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/03_PPPD.pdf?job=build-docs)
- Infrastructure Attribute Policy (IAP): [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/04_IAP.pdf?job=build-docs)

## Templates

- Community AAI Acceptable Use Policy (CAAI-AUP):
    (english: [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/05-CAAI_AUP.pdf?job=build-docs),
    [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/05-CAAI_AUP.docx?job=build-docs)),
    (german: [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/05-CAAI_AUP_DE.pdf?job=build-docs),
    [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/05-CAAI_AUP_DE.docx?job=build-docs))
- VO Acceptable Use Policy (VO-AUP):
    ([pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/05-VO_AUP_template.pdf?job=build-docs),
    [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/05-VO_AUP_template.docx?job=build-docs))
    (german: [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/05-VO_AUP_template_DE.pdf?job=build-docs),
    [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/05-VO_AUP_template_DE.docx?job=build-docs))
- Service Acceptable Use Policy (SAUP):
    (english: [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/05a-Service_AUP_template.pdf?job=build-docs),
    [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/05a-Service_AUP_template.docx?job=build-docs)),
    (german: [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/05a-Service_AUP_template_DE.pdf?job=build-docs),
    [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/05a-Service_AUP_template_DE.docx?job=build-docs))
- Service Access Policy (SAP):
    (english: [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/06_SAP_template.pdf?job=build-docs),
    [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/06_SAP_template.docx?job=build-docs))
- VO Lifecycle Policy:
    (english: [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/91_VO-lifecycle.pdf?job=build-docs),
    [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/91_VO-lifecycle.docx?job=build-docs))
- Privacy Statement Template
    (english: [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/07_Privacy_Policy_Template.pdf?job=build-docs),
    [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/07_Privacy_Policy_Template.docx?job=build-docs)),
    (german: [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/07_Privacy_Policy_Template_DE.pdf?job=build-docs),
    [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/07_Privacy_Policy_Template_DE.docx?job=build-docs))
- Template Verfahrensverzeichnis:
    [odt](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/master/raw/Template_Verfahrensverzeichnis_AAI.odt?job=build-docs)

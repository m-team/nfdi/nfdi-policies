---
documentclass: article
title: Service Access Policy Template
colorlinks: true
fontsize: 11pt
geometry: margin=20mm
papersize: a4
numbersections: true
header-includes:
    - # font
    - \renewcommand{\familydefault}{\sfdefault}

    - # linespacing
    - \usepackage{setspace}
    - \linespread{1.0}
    - \pagestyle{empty}

    - # footer
    - \pagenumbering{gobble}
    - \usepackage[markcase=noupper]{scrlayer-scrpage}
    - \cfoot*{\scriptsize\textcolor{gray}{{© Owned by the authors and made available under license{:} \href{https://creativecommons.org/licenses/by-nc-sa/4.0/}{https://creativecommons.org/licenses/by-nc-sa/4.0/} \\ Other Sources / Attribution / Acknowledgements{:} "The AARC Policy on the Processing of Personal Data", under CC BY-NC-SA 4.0 }}}

    - # logo on top right of first page
    - \usepackage{graphicx}
    - \graphicspath{{images/}}
    - \ohead*{\vspace{-18mm}\includegraphics[width=68.8mm,height=24.6mm]{nfdi-logo.png}}
    - \ohead{}
---
<!-- Add line numbers with: -->
<!-- - \usepackage{lineno} -->
<!-- - \linenumbers -->

<!-- Compile with:
pandoc <filename.md> -o <filename.pdf>
pandoc <filename.md> -o <filename.docx> --reference-doc style.docx
pandoc <filename.md> -o <filename.odt>  ## missing header and footer!
-->

(Version 0.9.4, August 19^th^, 2024)

# VO Registration

*The VO registration procedure is initiated by the VO Manager.*

## Requirements for VO Manager

- Anyone who can authenticate with assurance level RAF Cappuccino, cf.
  [[https://refeds.org/assurance]{.underline}](https://refeds.org/assurance)

- Member of one of the consortia of the National Research Data
  Infrastructure (NFDI) or any other organisational unit within the
  NFDI.

## VO registration form

VO Registrants need to provide these information:

- VO name
  - naming scheme and other requirements should be mentioned here
- VO description / purpose
  - this is (among others) to avoid duplicate VOs for the same effort
  - the VO Manager must have access to information on existing VOs
- VO AUP
- VO contact information:
  - VO Manager (Name, Postal Address, Email, Telephone)
  - VO Security Contact (Name, Postal Address, Email, Telephone)

## Policies to provide (Templates are provided)

- Acceptable Use Policy (AUP) (Template: \<insert link\>)
- Privacy Policy (Template: \<insert link\>, or use the Proxy PP)

## Policies to read and accept

- Top Level Infrastructure Policy
- VO Membership Management Policy
- Security Incident Response Procedure
- Policy on the Processing of Personal Data

*According to the Top-Level Infrastructure Policy, the VO Supervisor
evaluates the registration request and decides whether to accept or
reject it.*

## Acceptance criteria (all must be met)

- there is no existing VO with significantly overlapping goals.
- the VO registration form contains correct and complete data.
- the VO has provided, read and accepted all the required policies.

# VO Deregistration

## The VO deregistration can be initiated by

- VO Manager
- VO Supervisor
- VO User
- Infrastructure Management

## VO deregistration form

- Role of the requester (from list above)
- Reason for deregistration request
- Proposed timeline for decommissioning
- Assessment of the VO activities in the last 12 months (only if
    requester is not VO Manager)

*The VO Supervisor evaluates the deregistration request and decides
whether to accept or reject it. If the requester is the VO Manager, then
the request is automatically accepted.*

## Acceptance Criteria when the requester is not the VO Manager (all must be met):

- the VO has not produced accounting data for more than one year

- the VO Manager has been notified and given a deadline (minimum 1
    month) to respond

- the VO Manager has agreed to the deregistration request or the VO
    Manager has not responded to the request for feedback before the
    specified deadline

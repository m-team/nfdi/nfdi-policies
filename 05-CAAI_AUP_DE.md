---
documentclass: article
title: CAAI Acceptable Use Policy DE
colorlinks: true
fontsize: 11pt
geometry: margin=20mm
papersize: a4
numbersections: true
header-includes:
    - # font
    - \renewcommand{\familydefault}{\sfdefault}

    - # linespacing
    - \usepackage{setspace}
    - \linespread{1.0}
    - \pagestyle{empty}

    - # footer
    - \pagenumbering{gobble}
    - \usepackage[markcase=noupper]{scrlayer-scrpage}
    - \cfoot*{\scriptsize\textcolor{gray}{{© Owned by the authors and made available under license{:} \href{https://creativecommons.org/licenses/by-nc-sa/4.0/}{https://creativecommons.org/licenses/by-nc-sa/4.0/} \\ Other Sources / Attribution / Acknowledgements{:} "The AARC Policy on the Processing of Personal Data", under CC BY-NC-SA 4.0 }}}

    - # logo on top right of first page
    - \usepackage{graphicx}
    - \graphicspath{{images/}}
    - \ohead*{\vspace{-18mm}\includegraphics[width=68.8mm,height=24.6mm]{nfdi-logo.png}}
    - \ohead{}
---
<!-- Add line numbers with: -->
<!-- - \usepackage{lineno} -->
<!-- - \linenumbers -->

<!-- Compile with:
pandoc <filename.md> -o <filename.pdf>
pandoc <filename.md> -o <filename.docx> --reference-doc style.docx
pandoc <filename.md> -o <filename.odt>  ## missing header and footer!
-->

(Version 0.9.4, August 19^th^, 2024)

# Nutzungsbedingungen und Richtlinien zur zulässigen Nutzung (Acceptable Use Policy, "AUP")

Diese Bedingungen und Richtlinien legen die Regeln für
den Zugriff auf und die Nutzung von Ressourcen und Diensten ("Dienste") dar,
die von der NFDI-Infrastruktur für die Belange der Fachkonsortieren
und der von diesen vertretenen wissenschaftlichen Communities bereitgestellt
werden. Dies betrifft unter anderem die Übertragung, Verarbeitung und
Speicherung von Daten.

Indem Sie sich als Nutzer:in registrieren, erklären Sie, dass Sie die folgenden
Nutzungsbedingungen gelesen und verstanden haben und diese auch einhalten werden:

1. Sie dürfen die Dienste nur in einer Weise nutzen, die mit den
   hier beschriebenen Zwecken und Beschränkungen im Einklang steht;
   Sie müssen auf andere Nutzende Rücksicht nehmen, unter anderem
   indem Sie den Diensten keinen Schaden zufügen; Sie sind verpflichtet,
   bei der Lösung von Problemen, die sich aus Ihrer Nutzung der Dienste
   ergeben, mitzuwirken.
2. Sie dürfen die Dienste nur für gesetzeskonforme Zwecke nutzen und dürfen
   weder gegen administrative oder Sicherheitsmechanismen verstoßen noch
   versuchen, diese zu umgehen.
3. Sie sind verpflichtet, geistiges Eigentum und Vertraulichkeitsvereinbarungen zu respektieren.
4. Sie sind verpflichtet, Ihre Zugangsdaten zu schützen und sicher zu verwahren (z.B. Passwörter,
   private Schlüssel oder Multi-Faktor Tokens); eine Weitergabe an Dritte ist nicht gestattet.
5. Sie sind verpflichtet, die Sie betreffenden Daten korrekt und auf dem neuesten Stand halten.
6. Sie sind verpflichtet, bekannte oder vermutete Sicherheitsverletzungen unverzüglich
   zu melden sowie Kompromittierung oder Missbrauch von Zugangsdaten an die unten
   angegebene Kontaktadresse für Sicherheitsfragen zu melden. Die Kompromittierung von
   Zugangsdaten ist an die jeweiligen Aussteller zu melden.
7. Die Inanspruchnahme der Dienste erfolgt nur in dem Umfang, der in den unten referenzierten
   Service Level Agreements festgelegt ist. Die Nutzung ohne solche Vereinbarungen erfolgt
   ohne Gewähr.
8. Die Verarbeitung Ihrer personenbezogener Daten erfolgt gemäß der unten referenzierten
   Datenschutzerklärung.
9. Ihre Nutzung der Dienste kann aus administrativen, betrieblichen oder
   sicherheitstechnischen Gründen ohne vorherige Ankündigung und ohne Anspruch auf
   Entschädigung eingeschränkt oder ausgesetzt werden.
10. Bei Verstößen gegen diese Regeln können Sie für die Folgen verantwortlich gemacht werden.
   Unter anderem kann Ihr Nutzerkonto gesperrt werden und eine Meldung an Ihre
   Heimatorganisation oder die zuständige Strafverfolgungsbehörde erfolgen.
11. Sie sind verpflichtet, die Nutzung der zur Verfügung gestellten Ressourcen/Dienste
   in angemessener Weise zu bekunden oder zu zitieren, gemäß den Vorgaben der Stelle(n),
   die Ihnen Zugang gewährt/gewähren.

Administrativer Kontakt: \{E-Mail-Adresse der zuständigen Community, Behörde
oder Infrastruktur\}

Kontakt für Sicherheitsfragen: \{E-Mail-Adresse der für Sicherheitsfragen zuständigen Stelle
der betreffenden Community, Behörde oder Infrastruktur\}

Datenschutzerklärung: \{URL\}

Service Level Agreement(s): \{URLs\}

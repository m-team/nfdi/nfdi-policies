---
documentclass: article
title: Policy on the Processing of Personal Data
colorlinks: true
fontsize: 11pt
geometry: margin=20mm
papersize: a4
numbersections: true
header-includes:
    - # font
    - \renewcommand{\familydefault}{\sfdefault}

    - # linespacing
    - \usepackage{setspace}
    - \linespread{1.0}
    - \pagestyle{empty}

    - # footer
    - \pagenumbering{gobble}
    - \usepackage[markcase=noupper]{scrlayer-scrpage}
    - \cfoot*{\scriptsize\textcolor{gray}{{© Owned by the authors and made available under license{:} \href{https://creativecommons.org/licenses/by-nc-sa/4.0/}{https://creativecommons.org/licenses/by-nc-sa/4.0/} \\ Other Sources / Attribution / Acknowledgements{:} "The AARC Policy on the Processing of Personal Data", under CC BY-NC-SA 4.0 }}}

    - # logo on top right of first page
    - \usepackage{graphicx}
    - \graphicspath{{images/}}
    - \ohead*{\vspace{-18mm}\includegraphics[width=68.8mm,height=24.6mm]{nfdi-logo.png}}
    - \ohead{}
---
<!-- Add line numbers with: -->
<!-- - \usepackage{lineno} -->
<!-- - \linenumbers -->

<!-- Compile with:
pandoc <filename.md> -o <filename.pdf>
pandoc <filename.md> -o <filename.docx> --reference-doc style.docx
pandoc <filename.md> -o <filename.odt>  ## missing header and footer!
-->

(Version 0.9.4, August 19^th^, 2024)

# INTRODUCTION

This policy ensures that data collected as a result of the use of the
Infrastructure is processed fairly and lawfully by Infrastructure
participants. Some of this data, for example that relating to user
registration, monitoring and accounting contains "personal data" as
defined by the European Union (EU) \[GDPR\]. The collection and
processing of personal data is subject to restrictions aimed at
protecting the privacy of individuals.

# DEFINITIONS

-   *Personal Data*: any information relating to an identified or
    identifiable natural person \[GDPR\].

-   *Processing (Processed)*: any operation or set of operations,
    including collection and storage, which is performed upon Personal
    Data \[GDPR\].

-   *Controller*: the natural or legal person, public authority, agency
    or other body which, alone or jointly with others, determines the
    purposes and means of the Processing of Personal Data[^1] \[GDPR\]
    on behalf of an Infrastructure Participant.

-   *Processor*: a natural or legal person, public authority, agency or
    other body which Processes Personal Data on behalf of the Controller
    \[GDPR\].

# SCOPE

This policy covers Personal Data that is Processed as a prerequisite for
or as a result of a User's use of Infrastructure services. Examples of
such Personal Data include registration information, credential
identifiers and usage, accounting, security and monitoring records.

This policy does not cover Personal Data relating to third parties
included in datasets provided by the User or the research community to
which they belong as part of their research activity. Examples of such
data are medical data sets which may contain Personal Data.

# POLICY

By their activity in the Infrastructure, Participants:

-   Declare that they have read, understood and will abide by the
    Principles of Personal Data Processing as set out below.

-   Declare their acknowledgment that failure to abide by these
    Principles may result in exclusion from the Infrastructure, and that
    if such failure is thought to be the result of an unlawful act or
    results in unlawful information disclosure, they may be reported to
    the relevant legal authorities.

-   Abide by the REFEDS Code of Conduct for Service Providers \[CoCo\].

# PRINCIPLES OF PERSONAL DATA PROCESSING

```{=html}
<!-- -->
```
1.  The User whose Personal Data is being Processed shall be treated
    fairly and in an open and transparent manner.

2.  Personal Data of Users (hereinafter "Personal Data") shall be
    Processed only for those administrative, operational, accounting,
    monitoring and security purposes that are necessary for the safe and
    reliable operation of Infrastructure services, without prejudice to
    the Users' rights under the relevant laws.

3.  Processing of Personal Data shall be adequate, relevant and not
    excessive in relation to the purposes for which they are Processed.

4.  Personal Data shall be accurate and, where necessary, kept up to
    date. Where Personal Data are found to be inaccurate or incomplete,
    having regard to the purposes for which they are Processed, they
    shall be rectified or purged.

5.  Personal Data Processed for the purposes listed under paragraph 2
    above shall not be kept for longer than the period defined in a
    relevant Infrastructure service policy governing the type of
    Personal Data record being Processed (e.g. registration, monitoring
    or accounting).

6.  Appropriate technical and organisational measures shall be taken
    against unauthorised disclosure or Processing of Personal Data and
    against accidental loss or destruction of, or damage to, Personal
    Data. As a minimum, Infrastructure Participants shall:

    a.  Restrict access to stored Personal Data under their control to
        appropriate authorised individuals;

    b.  Transmit Personal Data by network or other means in a manner to
        prevent disclosure to unauthorised individuals;

    c.  Not disclose Personal Data unless in accordance with these
        Principles of Personal Data Processing;

    d.  Appoint at least one Data Protection Officer (DPO) to which
        Users or other Infrastructure Participants can report suspected
        breaches of this policy;

    e.  Respond to suspected breaches of this Policy promptly and
        effectively and take the appropriate action where a breach is
        found to have occurred;

    f.  Define periodic audit intervals and procedures to ensure
        compliance to this Policy and make the results of such audits
        available to other Infrastructure Participants upon their
        request.

7.  Each Infrastructure service interface provided for the User must
    provide, in a visible and accessible way, a Privacy Policy
    containing the following elements:

    a.  Name and contact details of the Controller responsible for
        Processing Personal Data;

    b.  Description of Personal Data being Processed;

    c.  Purpose or purposes of Processing of Personal Data as well as
        the legal basis for the processing;

    d.  Third party recipients of the personal data, if any; as well as
        the existence or absence of adequacy appropriate or suitable
        safeguards in case the recipient is not bound to GDPR.

    e.  Retention period of the Personal Data Processed;

    f.  Explanation of the rights of the Users according to GDPR;

    g.  The contact details of the Controller's DPO to which the User
        should direct requests in relation to their rights above;

    h.  Reference to this Policy.

> For this purpose, the NFDI-AAI Privacy Policy Template (PP) should be
> used.

8.  Personal Data may only be transferred to or otherwise shared with
    individuals or organisations where the recipient:

    a.  has agreed to be bound by this Policy and the set of common
        Infrastructure policies, or

    b.  is part of a recognised Computer Incident Response Team
        framework and as part of an incident investigation to prevent
        active or suspected misuse of Infrastructure services, or

    c.  presents an appropriately enforced legal request.

```{=html}
<!-- -->
```
# REFERENCES

\[CoCo\] REFEDS Data Protection Code of Conduct -
[[https://refeds.org/category/code-of-conduct]{.underline}](https://refeds.org/category/code-of-conduct)

\[GDPR\] EU General Data Protection Regulation -\
[[https://www.consilium.europa.eu/en/policies/data-protection/data-protection-regulation/]{.underline}](https://www.consilium.europa.eu/en/policies/data-protection/data-protection-regulation/)

[^1]: The principles of data protection should not apply to anonymous
    information, namely information which does not relate to an
    identified or identifiable natural person or to personal data
    rendered anonymous in such a manner that the data subject is not or
    no longer identifiable. This policy does not therefore concern the
    processing of such anonymous information, including for statistical
    or research purposes.

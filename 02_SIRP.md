---
documentclass: article
title: Security Incident Response Procedure
colorlinks: true
fontsize: 11pt
geometry: margin=20mm
papersize: a4
numbersections: true
header-includes:
    - # font
    - \renewcommand{\familydefault}{\sfdefault}

    - # linespacing
    - \usepackage{setspace}
    - \linespread{1.0}
    - \pagestyle{empty}

    - # footer
    - \pagenumbering{gobble}
    - \usepackage[markcase=noupper]{scrlayer-scrpage}
    - \cfoot*{\scriptsize\textcolor{gray}{{© Owned by the authors and made available under license{:} \href{https://creativecommons.org/licenses/by-nc-sa/4.0/}{https://creativecommons.org/licenses/by-nc-sa/4.0/} \\ Other Sources / Attribution / Acknowledgements{:} "The AARC Policy on the Processing of Personal Data", under CC BY-NC-SA 4.0 }}}

    - # logo on top right of first page
    - \usepackage{graphicx}
    - \graphicspath{{images/}}
    - \ohead*{\vspace{-18mm}\includegraphics[width=68.8mm,height=24.6mm]{nfdi-logo.png}}
    - \ohead{}
---
<!-- Add line numbers with: -->
<!-- - \usepackage{lineno} -->
<!-- - \linenumbers -->

<!-- Compile with:
pandoc <filename.md> -o <filename.pdf>
pandoc <filename.md> -o <filename.docx> --reference-doc style.docx
pandoc <filename.md> -o <filename.odt>  ## missing header and footer!
-->

This procedure applies for any suspected or confirmed security breach
with a potential impact on the *Infrastructure* or on other
*Infrastructure* participants. Hereinafter, the term "participants" only
includes those *Infrastructure* participants that are required to abide
by this procedure which is based on the AARC Guide to Federated Security
Incident Response for Research \[1\]

(Version 0.9.4, August 19^th^, 2024)

**Security Incident Response Procedure for Infrastructure Participants**

1.  Contain the security incident to avoid further propagation whilst
    aiming at carefully preserving evidence and logs. Record all actions
    taken, along with an accurate timestamp.

2.  Report the security incident to the Infrastructure Security Contact
    within one local working day of the initial discovery or
    notification of the security incident.

3.  In collaboration with the Infrastructure Security Contact, ensure
    all affected participants in the Infrastructure and federation (and,
    if applicable, in other federations), are notified via their
    security contact with a "heads-up" and can take action. In the case
    of a personal data breach, report the security incident to the
    Controller to ensure compliance with applicable GDPR provisions, and
    share additional information as necessary with all affected
    participants to enable them to comply with applicable GDPR
    provisions as well.

4.  Announce suspension of service (if applicable) in accordance with
    Infrastructure, federation and inter-federation practices, cf.
    https://doku.tid.dfn.de/de:aai:incidentresponse

5.  Perform appropriate investigation, system analysis and forensics,
    and strive to understand the cause of the security incident, as well
    as its full extent. Identifying the cause of security incidents is
    essential to prevent them from reoccurring. The time and effort
    needs to be commensurate with the scale of the problem and with the
    potential damage and risks faced by affected participants.

6.  Share additional information as often as necessary to keep all
    affected participants up-to-date with the status of the security
    incident and enable them to investigate and take action should new
    information appear.

7.  Respond to requests for assistance from other participants involved
    in the security incident within one working day.

8.  Take corrective action, restore access to service (if applicable)
    and legitimate user access.

9.  In collaboration with the Security Incident Response Coordinator,
    produce and share a report of the incident with all Sirtfi-compliant
    organisations \[2\] in all affected federations within one month.
    This report should be labeled TLP AMBER \[3\] or higher.

10. Update documentation and procedures as necessary.

**Security Incident Response Procedure for the Infrastructure Security
Contact**

1.  Assist Infrastructure participants in performing appropriate
    investigation, system analysis and forensics, and strive to
    understand the cause of the security incident, as well as its full
    extent. The time and effort needs to be commensurate with the scale
    of the problem and with the potential damage and risks faced by
    affected participants.

2.  Report the security incident to their federation security contact
    point within one local working day of the initial discovery or
    notification of the security incident.

3.  Ensure all affected participants in the Infrastructure and
    federation (and, if applicable, in other federations) are notified
    via their security contact with a "heads-up" within one local
    working day. If other federations are affected, the eduGAIN security
    contact point must be notified, even if affected participants in all
    other federations have been contacted directly.

4.  Coordinate the security incident resolution process and
    communication with affected participants until the security incident
    is resolved.

5.  Ensure suspension of service (if applicable) is announced in
    accordance with Infrastructure, federation and inter-federation
    practices.

6.  Share additional information as often as necessary to keep all
    affected participants up-to-date with the status of the security
    incident and enable them to investigate and take action should new
    information appear.

7.  Assist and advise participants in taking corrective action, or
    restoring access to service (if applicable) and legitimate user
    access.

8.  Produce and share a report of the incident with all Sirtfi-compliant
    organisations in all affected federations within one month. This
    report should be labeled TLP AMBER \[3\] or higher.

9.  Update documentation and procedures as necessary.

**References**

\[1\] AARC Deliverable I051: Guide to Federated Security Incident
Response for Research -
[[https://wiki.geant.org/display/AARC/Generic+Security+Incident+Response+Procedure]{.underline}](https://wiki.geant.org/display/AARC/Generic+Security+Incident+Response+Procedure)

\[2\] Security Incident Response Trust Framework for Federated Identity
(Sirtfi) -
[[https://refeds.org/sirtfi]{.underline}](https://refeds.org/sirtfi)

\[3\] Traffic Light Protocol -
[[https://www.us-cert.gov/tlp]{.underline}](https://www.us-cert.gov/tlp)
